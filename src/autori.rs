fn main() {
    let stdin = std::io::stdin();
    let mut first_line = String::new();
    let _ = stdin.read_line(&mut first_line);

    println!("{}", get_short_variation(&first_line));
}

fn get_short_variation(long_variation: &str) -> String {
    long_variation
        .chars()
        .filter(|c| c.is_uppercase())
        .collect()
}

#[cfg(test)]
#[macro_use]
extern crate proptest;
#[cfg(test)]
mod tests {
    use super::*;
    extern crate assert_cmd;

    proptest! {
        #[test]
        fn short_variation_is_all_caps(ref long_variation in "([A-Z][a-z]*)(-([A-Z][a-z]*))*") {
            let short_variation = get_short_variation(long_variation);
            assert!(short_variation.chars().all(|c| c.is_uppercase()));
        }
    }
}
