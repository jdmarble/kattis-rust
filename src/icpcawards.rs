use std::io::{self, BufRead};
use std::collections::HashSet;

fn main() {
    let stdin = io::stdin();

    let mut first_line = String::new();
    let _error = stdin.read_line(&mut first_line);
    let _count: u8 = first_line.trim().parse().unwrap();

    let mut top_universities = HashSet::new();

    for line in stdin.lock().lines().map(|l| l.unwrap()) {
        let mut tokens = line.split_whitespace();
        let university = tokens.next().unwrap().trim().to_string();

        if top_universities.insert(university) {
            println!("{}", line);
        }
        if top_universities.len() >= 12 {
            break;
        }
    }
}

