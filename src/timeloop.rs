fn main() {
    let mut first_line = String::new();
    let _ = std::io::stdin().read_line(&mut first_line);
    let count: u8 = first_line.trim().parse().unwrap();

    for i in 1..=count {
        println!("{} Abracadabra", i);
    }
}
