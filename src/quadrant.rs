use std::io::BufRead;

fn main() {
    let stdin = std::io::stdin();
    let mut lines = stdin.lock().lines();
    let x: i16 = lines.next().unwrap().unwrap().trim().parse().unwrap();
    let y: i16 = lines.next().unwrap().unwrap().trim().parse().unwrap();

    println!("{}", quadrant(x, y));
}

fn quadrant(x: i16, y: i16) -> u8 {
    match (x.signum(), y.signum()) {
        (1, 1) => 1,
        (-1, 1) => 2,
        (-1, -1) => 3,
        (1, -1) => 4,
        _ => 0,
    }
}

#[cfg(test)]
#[macro_use]
extern crate proptest;
#[cfg(test)]
mod tests {
    use super::*;
    extern crate assert_cmd;

    proptest! {
        #[test]
        fn quadrant_prop(q in 1u8..4, xa in 1i16..1000, ya in 1i16..1000) {
            let x = match q {
                1 | 4 => xa,
                2 | 3 => -xa,
                _ => 0
            };
            let y = match q {
                1 | 2 => ya,
                3 | 4 => -ya,
                _ => 0
            };
            assert_eq!(q, quadrant(x, y));
        }
    }
}
