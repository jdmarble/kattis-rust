use std::io::{self, BufRead};

fn main() {
    let stdin = io::stdin();

    let mut first_line = String::new();
    let _ = stdin.read_line(&mut first_line);
    let mut tokens = first_line.split_whitespace();
    let _count: u8 = tokens.next().unwrap().trim().parse().unwrap();
    let w: u8 = tokens.next().unwrap().trim().parse().unwrap();
    let h: u8 = tokens.next().unwrap().trim().parse().unwrap();

    for line in stdin.lock().lines().map(|l| l.unwrap()) {
        let l: u16 = line.trim().parse().unwrap();
        println!(
            "{}",
            if match_fits_in_box(w, h, l) {
                "DA"
            } else {
                "NE"
            }
        );
    }
}

fn match_fits_in_box(w: u8, h: u8, l: u16) -> bool {
    let wi = u32::from(w);
    let hi = u32::from(h);
    let li = u32::from(l);
    wi * wi + hi * hi >= li * li
}

#[cfg(test)]
#[macro_use]
extern crate proptest;
#[cfg(test)]
mod tests {
    use super::*;
    extern crate assert_cmd;

    proptest! {
        #[test]
        fn match_fits_in_box_matches_model(w in 1u8..100, h in 1u8..100, l in 1u16..1000) {
            let wf = w as f64;
            let hf = h as f64;
            let lf = l as f64;
            assert_eq!((wf.powi(2) + hf.powi(2)).sqrt() >= lf, match_fits_in_box(w, h, l));
        }
    }
}
