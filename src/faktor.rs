use std::io;

fn main() {
    let stdin = io::stdin();
    let mut first_line = String::new();
    let _ = stdin.read_line(&mut first_line);
    let mut tokens = first_line.split_whitespace();
    let articles: u8 = tokens.next().unwrap().trim().parse().unwrap();
    let impact: u8 = tokens.next().unwrap().trim().parse().unwrap();

    println!("{}", min_citations_for_impact(articles, impact));
}

fn min_citations_for_impact(articles: u8, impact: u8) -> u16 {
    u16::from(articles) * (u16::from(impact - 1)) + 1
}

#[cfg(test)]
#[macro_use]
extern crate proptest;
#[cfg(test)]
mod tests {
    use super::*;
    extern crate assert_cmd;

    proptest! {
        #[test]
        fn citations_give_impact(articles in 1u8..100, impact in 1u8..100) {
            let citations = min_citations_for_impact(articles, impact);
            assert_eq!(impact as f64, ((citations as f64)/(articles as f64)).ceil());
        }
        #[test]
        fn fewer_citations_do_not_give_impact(articles in 1u8..100, impact in 1u8..100) {
            let citations = min_citations_for_impact(articles, impact) - 1;
            assert_ne!(impact as f64, ((citations as f64)/(articles as f64)).ceil());
        }
    }
}
