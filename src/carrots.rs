fn main() {
    let stdin = std::io::stdin();
    let mut first_line = String::new();
    let _ = stdin.read_line(&mut first_line);
    let mut tokens = first_line.split_whitespace();
    let problems: u16 = tokens.nth(1).unwrap().trim().parse().unwrap();

    println!("{}", problems);
}

