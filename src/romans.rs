fn main() {
    let stdin = std::io::stdin();
    let mut first_line = String::new();
    let _ = stdin.read_line(&mut first_line);
    let miles: f64 = first_line.trim().parse().unwrap();

    println!("{}", paces_in_a_mile(miles));
}

const FEET_IN_A_MILE: f64 = 5_280.0;
const FEET_IN_A_ROMAN_MILE: f64 = 4_854.0;
const PACES_IN_A_ROMAN_MILE: f64 = 1_000.0;

fn paces_in_a_mile(miles: f64) -> f64 {
    let feet = miles * FEET_IN_A_MILE;
    let roman_miles = feet / FEET_IN_A_ROMAN_MILE;
    (roman_miles * PACES_IN_A_ROMAN_MILE).round()
}

