fn main() {
    println!("Hello World!");
}

#[cfg(test)]
mod tests {
    extern crate assert_cmd;
    #[test]
    fn expected_stdout() {
        use std::process::Command;
        use tests::assert_cmd::prelude::*;

        let mut cmd = Command::cargo_bin("hello").unwrap();
        let assert = cmd.assert();
        assert.stdout("Hello World!\n");
    }
}
