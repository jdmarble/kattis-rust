use std::env;
use std::fs;
use std::io::Write;
use std::path::Path;

fn main() {
    let outfile_path = Path::new(&env::var("OUT_DIR").unwrap()).join("gen_tests.rs");
    let mut outfile = fs::File::create(outfile_path).unwrap();

    let problem_readdir = fs::read_dir("./samples").unwrap();
    for problem_direntry_maybe in problem_readdir {
        let problem_direntry = problem_direntry_maybe.unwrap();
        let problem = problem_direntry.file_name().into_string().unwrap();

        let sample_readdir = fs::read_dir(problem_direntry.path()).unwrap();
        for sample_direntry in sample_readdir {
            let sample_path = sample_direntry.unwrap().path();
            let sample_extension = sample_path.extension().unwrap().to_string_lossy();
            if sample_extension == "in" {
                let sample = sample_path.file_stem().unwrap().to_string_lossy();
                write!(
                    outfile,
                    r#"
                    #[test]
                    fn test_sample_{problem}_{sample}() {{
                        sample_in_results_in_sample_ans("{problem}", "{sample}");
                    }}
                "#,
                    problem = problem,
                    sample = sample
                )
                .unwrap();
            }
        }
    }
}
