extern crate assert_cmd;
use assert_cmd::prelude::*;
use std::fs::read;
use std::path::Path;
use std::process::Command;

fn sample_in_results_in_sample_ans(problem: &str, sample: &str) {
    let base_path = Path::new("./samples").join(problem).join(sample);
    let in_path = base_path.with_extension("in");
    let ans_path = base_path.with_extension("ans");

    let actual_out = Command::cargo_bin(problem)
        .unwrap()
        .with_stdin()
        .path(in_path)
        .unwrap()
        .output()
        .unwrap()
        .stdout;

    let expected_out = read(ans_path).unwrap();
    assert_eq!(
        String::from_utf8_lossy(&expected_out).trim(),
        String::from_utf8_lossy(&actual_out).trim()
    );
}

include!(concat!(env!("OUT_DIR"), "/gen_tests.rs"));
